# MJR Legends - Space Astronomy 2 - Fixed

## What is this about?
https://www.curseforge.com/minecraft/modpacks/space-astronomy-2

Space Astronomy 2, developed by MJR Legends is the best Minecraft modpack I have ever played.
When trying to install it through CurseForge in Prism Launcher, it complained about missing
mods and the Power Armor from Modular Powersuits was not functional in space, which made you
die due to high pressure.

This simple repository contains the missing mods, that you should copy into your 'mods' folder and
the new ExtraPlanets.cfg configuration file, which should make your Power Armor functional in Space.


## Clone the repo

```
git clone https://gitlab.com/SA1G0N/mjr-legends-space-astronomy-2-fixed.git
```

Add the following mods into the 'mods' folder in your Space Astronomy 2 instance, and ExtraPlanets.cfg
into your 'config' folder.

